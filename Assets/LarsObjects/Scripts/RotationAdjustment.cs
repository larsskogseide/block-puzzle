using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationAdjustment : MonoBehaviour
{
    public int timeDelay = 3;
    float xRotation;
    float yRotation;
    float zRotation;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RotateBlock());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator RotateBlock()
    {
        while(true)
        {
            if ((transform.rotation.eulerAngles.x > 0 && transform.rotation.eulerAngles.x < 45) || (transform.rotation.eulerAngles.x > 315 && transform.rotation.eulerAngles.x < 360))
            {
                xRotation = 0;
            }

            if ((transform.rotation.eulerAngles.x > 45 && transform.rotation.eulerAngles.x < 90) || (transform.rotation.eulerAngles.x > 90 && transform.rotation.eulerAngles.x < 135))
            {
                xRotation = 90;
            }

            if ((transform.rotation.eulerAngles.x > 135 && transform.rotation.eulerAngles.x < 180) || (transform.rotation.eulerAngles.x > 180 && transform.rotation.eulerAngles.x < 225))
            {
                xRotation = 180;
            }

            if ((transform.rotation.eulerAngles.x > 225 && transform.rotation.eulerAngles.x < 270) || (transform.rotation.eulerAngles.x > 270 && transform.rotation.eulerAngles.x < 315))
            {
                xRotation = 270;
            }

            if ((transform.rotation.eulerAngles.y > 0 && transform.rotation.eulerAngles.y < 45) || (transform.rotation.eulerAngles.y > 315 && transform.rotation.eulerAngles.y < 360))
            {
                yRotation = 0;
            }

            if ((transform.rotation.eulerAngles.y > 45 && transform.rotation.eulerAngles.y < 90) || (transform.rotation.eulerAngles.y > 90 && transform.rotation.eulerAngles.y < 135))
            {
                yRotation = 90;
            }

            if ((transform.rotation.eulerAngles.y > 135 && transform.rotation.eulerAngles.y < 180) || (transform.rotation.eulerAngles.y > 180 && transform.rotation.eulerAngles.y < 225))
            {
                yRotation = 180;
            }

            if ((transform.rotation.eulerAngles.y > 225 && transform.rotation.eulerAngles.y < 270) || (transform.rotation.eulerAngles.y > 270 && transform.rotation.eulerAngles.y < 315))
            {
                yRotation = 270;
            }

            if ((transform.rotation.eulerAngles.z > 0 && transform.rotation.eulerAngles.z < 45) || (transform.rotation.eulerAngles.z > 315 && transform.rotation.eulerAngles.z < 360))
            {
                zRotation = 0;
            }

            if ((transform.rotation.eulerAngles.z > 45 && transform.rotation.eulerAngles.z < 90) || (transform.rotation.eulerAngles.z > 90 && transform.rotation.eulerAngles.z < 135))
            {
                zRotation = 90;
            }

            if ((transform.rotation.eulerAngles.z > 135 && transform.rotation.eulerAngles.z < 180) || (transform.rotation.eulerAngles.z > 180 && transform.rotation.eulerAngles.z < 225))
            {
                zRotation = 180;
            }

            if ((transform.rotation.eulerAngles.z > 225 && transform.rotation.eulerAngles.z < 270) || (transform.rotation.eulerAngles.z > 270 && transform.rotation.eulerAngles.z < 315))
            {
                zRotation = 270;
            }

            //Debug.Log(new Vector3(xRotation, yRotation, zRotation));
            this.transform.eulerAngles = new Vector3(xRotation, yRotation, zRotation);
            yield return new WaitForSeconds(timeDelay);
        }
    }
}
