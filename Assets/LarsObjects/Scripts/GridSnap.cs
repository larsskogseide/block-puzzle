using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSnap : MonoBehaviour
{
    public float gridSizeX = 0.1f;
    public float gridSizeZ = 0.1f;
    public int rotationDegrees = 90;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var position = new Vector3(Mathf.Round(transform.position.x / gridSizeX) * gridSizeX,
                                   transform.position.y,
                                   Mathf.RoundToInt(transform.position.z / gridSizeZ) * gridSizeZ);

        transform.position = position;

        Debug.Log(transform.position);

        /*transform.eulerAngles = new Vector3(Mathf.Round(transform.localEulerAngles.x / rotationDegrees) * rotationDegrees,
                                            Mathf.Round(transform.localEulerAngles.y / rotationDegrees) * rotationDegrees,
                                            Mathf.Round(transform.localEulerAngles.z / rotationDegrees) * rotationDegrees);*/
    }
}
