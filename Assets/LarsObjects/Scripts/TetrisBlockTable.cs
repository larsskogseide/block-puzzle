using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisBlockTable : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Block")
        {
            //other.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
            
            //blocks sit on table so blocks will rotate as table rotates
            other.transform.parent = this.transform;
            //Debug.Log("collide");
        }
    }
}
