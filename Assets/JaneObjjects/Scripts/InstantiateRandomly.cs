using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateRandomly : MonoBehaviour
{

    public Transform pos;

    public GameObject[] objectsToInstantiate;  // a mount of objects I can spawn with array 

    public GameObject spawnButton;

    // Start is called before the first frame update
    void Start()
    {
       // int n = Random.Range(0,objectsToInstantiate.Length); // random spawn of these objects // objectsToInstantiate.Length= gbt die L�nge der Arrays an ohne magical number

      //  GameObject g = Instantiate(objectsToInstantiate[n], pos.position, objectsToInstantiate[n].transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnBlock()
    {
        int n = Random.Range(0, objectsToInstantiate.Length); // random spawn of these objects // objectsToInstantiate.Length= gbt die L�nge der Arrays an ohne magical number

        GameObject g = Instantiate(objectsToInstantiate[n], pos.position, objectsToInstantiate[n].transform.rotation);

        //change color of button when pressed
        StartCoroutine(ChangeColor());
    }

    private IEnumerator ChangeColor()
    {
        spawnButton.GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.1f);
        spawnButton.GetComponent<Renderer>().material.color = Color.red;
    }
}
